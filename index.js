var Utils = require('./lib/utils');

exports.copy = Utils.copy;
exports.installHooks = Utils.installHooks;
exports.findGitRoot = Utils.findGitRoot;
