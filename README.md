Precommit hook
--------------

# What is it?

This repo is based upon an old version of [precommit-hook](https://github.com/nlf/precommit-hook.git).
It has been adapted to keep it simple and to get it to work on Mac computers and in a way to allow developers to get
this to work with their repos while still allowing them to be deployed on Heroku.

# WARNING: If you already have a `.git/hooks/pre-commit` file, this package will overwrite it.

# Installation

Install this repo in your project in the usual manner. Since this is not published, incorporate the git package into your repo:

```
"devDependencies": {
    "simple-precommit-hook": "git+https://github.com/robhicks/simple-precommit-hook.git"
  }
```

Also add a pre-commit array to your package.json file:
```
"pre-commit": [
    "test"
  ]
```
Include whatever scripts you want to run. The scripts must be included in the package.json scripts array, e.g.:
```
"scripts": {
    "test": "./bin/tests"
  }
```
